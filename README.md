# SpTIndex

R* variant indexing library for generic spatio-temporal data using hybrid (memory + persistent store) storage model.